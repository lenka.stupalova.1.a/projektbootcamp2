package com.cgi.data;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.sql.DataSource;

import com.cgi.api.ContactDetailedSingleViewDto;
import com.cgi.api.ContactTypesCount;
import com.cgi.api.ContactViewDto;
import com.cgi.data.exceptions.DataAccessException;

@Stateless
public class ContactDao {

	@Resource(lookup = "jdbc/bootcampCGI")
	private DataSource dataSource;

	public List<ContactViewDto> findAllDetailed() {

		List<ContactViewDto> contacts = new ArrayList<>();
		try (Connection connection = dataSource.getConnection();
				Statement statement = connection.createStatement();
				ResultSet rs = statement.executeQuery(
						"SELECT * FROM contact c LEFT JOIN contact_type ct ON c.id_contact_type=ct.id_contact_type")) {

			while (rs.next()) {
				ContactViewDto contact = new ContactViewDto();
				contact.setContact(rs.getString("contact"));
				contact.setId_contact(rs.getInt("id_contact"));
				contact.setId_user(rs.getInt("id_user"));
				contact.setTitle(rs.getString("ct.title"));
				contacts.add(contact);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return contacts;
	}

	public List<ContactViewDto> findAll() {

		List<ContactViewDto> contacts = new ArrayList<>();
		try (Connection connection = dataSource.getConnection();
				Statement statement = connection.createStatement();
				ResultSet rs = statement.executeQuery("SELECT * FROM contact c")) {

			while (rs.next()) {
				ContactViewDto contact = new ContactViewDto();
				contact.setContact(rs.getString("contact"));
				contact.setId_contact(rs.getInt("id_contact"));
				contact.setId_user(rs.getInt("id_user"));
				contacts.add(contact);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return contacts;
	}

	public ContactDetailedSingleViewDto findByIdDetailed(long id) {

		String findByIdSQL = "SELECT c.id_contact, c.contact, u.nickname, u.birthday, ct.title FROM contact c LEFT JOIN user u ON u.id_user=c.id_user LEFT JOIN contact_type ct ON c.id_contact_type=ct.id_contact_type WHERE c.id_contact = ?";
		try (Connection conn = dataSource.getConnection(); PreparedStatement ps = conn.prepareStatement(findByIdSQL);) {
			ps.setLong(1, id);
			try (ResultSet rs = ps.executeQuery()) {
				if (rs.next()) {
					ContactDetailedSingleViewDto contact = new ContactDetailedSingleViewDto();
					contact.setContact(rs.getString("c.contact"));
					contact.setId_contact(rs.getInt("c.id_contact"));
					contact.setNickname(rs.getString("u.nickname"));
					contact.setBirthday(rs.getDate("u.birthday"));
					contact.setTitle(rs.getString("ct.title"));

					return contact;
				}
			}
		} catch (SQLException e) {
			throw new DataAccessException("Contact with id: " + id + " not found.", e);
		}
		throw new DataAccessException("Contact with id: " + id + " not found.");

	}

	public List<ContactTypesCount> countTypesOfEmail() {

		List<ContactTypesCount> contacts = new ArrayList<>();
		try (Connection connection = dataSource.getConnection();
				Statement statement = connection.createStatement();
				ResultSet rs = statement.executeQuery(
						"SELECT c.id_contact_type, ct.title, COUNT(ct.title) AS count FROM contact c LEFT JOIN contact_type ct ON c.id_contact_type=ct.id_contact_type GROUP BY ct.title")) {
			while (rs.next()) {
				ContactTypesCount contact = new ContactTypesCount();
				contact.setId_contact(rs.getInt("id_contact_type"));
				contact.setCount(rs.getInt("count"));
				contact.setTitle(rs.getString("ct.title"));
				contacts.add(contact);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return contacts;
	}

}
