package com.cgi.data;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.sql.DataSource;

import com.cgi.api.ContactDetailedSingleViewDto;
import com.cgi.api.PersonBasicViewDto;
import com.cgi.api.PersonCreateViewDto;
import com.cgi.api.PersonDetailedViewDto;
import com.cgi.data.exceptions.DataAccessException;

@Stateless
public class PersonDao {

	@Resource(lookup = "jdbc/bootcampCGI")
	private DataSource dataSource;

	public List<PersonBasicViewDto> findAll() {

		List<PersonBasicViewDto> persons = new ArrayList<>();
		try (Connection connection = dataSource.getConnection();
				Statement statement = connection.createStatement();
				ResultSet rs = statement.executeQuery("SELECT id_user, surname, nickname, first_name FROM user u")) {

			while (rs.next()) {
				PersonBasicViewDto person = new PersonBasicViewDto();
				person.setId(rs.getLong("id_user"));
				person.setFamilyName(rs.getString("surname"));
				person.setGivenName(rs.getString("first_name"));
				person.setName(rs.getString("nickname"));
				persons.add(person);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return persons;
	}

	public boolean createUser(PersonCreateViewDto pcv) {

		try (Connection connection = dataSource.getConnection(); Statement statement = connection.createStatement();) {

			int zapis = statement.executeUpdate(
					"INSERT INTO user (nickname, first_name, surname, email, password, id_address) VALUES ('"
							+ pcv.getName() + "', '" + pcv.getGivenName() + "', '" + pcv.getFamilyName() + "', 'email"
							+ pcv.getName() + "', 'password', '1')");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return true; // podarilo sa
	}

	public PersonDetailedViewDto findById(Long id) {

		String findByIdSQL = "SELECT * FROM user u WHERE id_user = ?";
		try (Connection conn = dataSource.getConnection(); PreparedStatement ps = conn.prepareStatement(findByIdSQL);) {
			ps.setLong(1, id);
			try (ResultSet rs = ps.executeQuery()) {
				if (rs.next()) {
					PersonDetailedViewDto person = new PersonDetailedViewDto();
					person.setFamilyName(rs.getString("surname"));
					person.setGivenName(rs.getString("first_name"));
					person.setName(rs.getString("nickname"));
					person.setId(id);

					return person;
				}
			}
		} catch (SQLException e) {
			throw new DataAccessException("Contact with id: " + id + " not found.", e);
		}
		throw new DataAccessException("Contact with id: " + id + " not found.");

	}

	public boolean deleteUser(Long id) {

		///////////////////////////////////////////////////////////
		////Asi by bolo lepsie vymazat cez DELETE CASCADE///////////
		/////Ale nasla som vsetky foreign keys, nebolo vela tabuliek/////
		///////////////////////////////////////////////////////////
		System.out.println("mazem");
		String findByIdSQL = "DELETE FROM contact WHERE id_user = ?";
		try (Connection conn = dataSource.getConnection(); PreparedStatement ps = conn.prepareStatement(findByIdSQL);) {
			ps.setLong(1, id);
			int rs = ps.executeUpdate();
			//return true;

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		findByIdSQL = "DELETE FROM relationship WHERE id_user1 = ?";
		try (Connection conn = dataSource.getConnection(); PreparedStatement ps = conn.prepareStatement(findByIdSQL);) {
			ps.setLong(1, id);
			int rs = ps.executeUpdate();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		findByIdSQL = "DELETE FROM relationship WHERE id_user2 = ?";
		try (Connection conn = dataSource.getConnection(); PreparedStatement ps = conn.prepareStatement(findByIdSQL);) {
			ps.setLong(1, id);
			int rs = ps.executeUpdate();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		findByIdSQL = "DELETE FROM role_has_user WHERE id_user = ?";
		try (Connection conn = dataSource.getConnection(); PreparedStatement ps = conn.prepareStatement(findByIdSQL);) {
			ps.setLong(1, id);
			int rs = ps.executeUpdate();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
		
		findByIdSQL = "DELETE FROM user_has_meeting WHERE id_user = ?";
		try (Connection conn = dataSource.getConnection(); PreparedStatement ps = conn.prepareStatement(findByIdSQL);) {
			ps.setLong(1, id);
			int rs = ps.executeUpdate();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		findByIdSQL = "DELETE FROM user WHERE id_user = ?";
		try (Connection conn = dataSource.getConnection(); PreparedStatement ps = conn.prepareStatement(findByIdSQL);) {
			ps.setLong(1, id);
			int rs = ps.executeUpdate();
			return true;

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		throw new DataAccessException("Contact with id: " + id + " not found.");

	}

	public boolean updateUser(Long id, String surname) {

		try (Connection connection = dataSource.getConnection(); Statement statement = connection.createStatement();) {

			int zapis = statement.executeUpdate("UPDATE user SET surname = '"+ surname +"' WHERE id_user = " + id );
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return true; // podarilo sa
	}
}
