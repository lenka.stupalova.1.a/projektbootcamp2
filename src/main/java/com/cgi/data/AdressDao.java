package com.cgi.data;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.sql.DataSource;

import com.cgi.api.AdressBasicViewAto;
import com.cgi.api.AdressPersonGroupByCity;

@Stateless
public class AdressDao {

	@Resource(lookup = "jdbc/bootcampCGI")
	private DataSource dataSource;

	public List<AdressBasicViewAto> findAll() {

		List<AdressBasicViewAto> adresses = new ArrayList<>();
		try (Connection connection = dataSource.getConnection();
				Statement statement = connection.createStatement();
				ResultSet rs = statement.executeQuery("SELECT * FROM address a")) {

			while (rs.next()) {
				AdressBasicViewAto adress = new AdressBasicViewAto();
				adress.setId_address(rs.getLong("id_address"));
				adress.setCity(rs.getString("city"));
				adress.setHouse_number(rs.getInt("house_number"));
				adress.setStreet(rs.getString("street"));
				adress.setZip_code(rs.getString("zip_code"));

				adresses.add(adress);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return adresses;
	}

	public List<AdressPersonGroupByCity> findCounts() {
		List<AdressPersonGroupByCity> adresses = new ArrayList<>();
		try (Connection connection = dataSource.getConnection();
				Statement statement = connection.createStatement();
				ResultSet rs = statement.executeQuery(
						"SELECT city, COUNT(u.id_user) AS count FROM address a LEFT JOIN user u ON a.id_address=u.id_address GROUP BY city")) {

			while (rs.next()) {
				AdressPersonGroupByCity adress = new AdressPersonGroupByCity();
				adress.setUserCount(rs.getInt("count"));
				adress.setCity(rs.getString("city"));

				adresses.add(adress);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return adresses;
	}
}
