package com.cgi.servlets;

import java.io.IOException;
import java.util.List;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cgi.api.AdressInfoView;
import com.cgi.service.AdressService;

/**
 * Servlet implementation class AdressInfoViewServlet
 */

@WebServlet(name = "AdressInfoViewServlet", urlPatterns = { "/adress-info" })
public class AdressInfoViewServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@EJB
	private AdressService adressService;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public AdressInfoViewServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		AdressInfoView aiv = adressService.adressInfoView();

		request.setAttribute("aiv", aiv.getAdressBasicViewAto());
		request.setAttribute("aiv2", aiv.getAdressPersonGroupByCity());
		request.getRequestDispatcher("/WEB-INF/jsp/adress/AdressInfoView.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
