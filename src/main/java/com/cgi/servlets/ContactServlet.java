package com.cgi.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cgi.api.ContactDetailedSingleViewDto;
import com.cgi.api.ContactTypesCount;
import com.cgi.api.ContactViewDto;
import com.cgi.service.ContactService;

/**
 * Servlet implementation class PersonListServlet
 */

@WebServlet(name = "ContactServlet", urlPatterns = { "/contact-list/*" })
public class ContactServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@EJB
	private ContactService contactService;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ContactServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String pathInfo = request.getServletPath() + request.getPathInfo();

		System.out.println("MOJE PATH INFO " + pathInfo);

		if (pathInfo != null && pathInfo.contains("/contact-types")) {
			System.out.println("pocitam typy");
			List<ContactTypesCount> contactTypesCount = contactService.countTypesOfEmail();
			request.setAttribute("contypes", contactTypesCount);
			request.getRequestDispatcher("/WEB-INF/jsp/contact/ContactTypes.jsp").forward(request, response);
		}
		else if (pathInfo != null && pathInfo.contains("/detail")) {
			System.out.println("Id...: " + request.getParameter("id"));
			String idStr = request.getParameter("id");
			Long idLong = Long.parseLong(idStr);
			System.out.println(idLong);
			ContactDetailedSingleViewDto contact = contactService.findByIdDetailed(idLong);
			request.setAttribute("contact", contact);
			request.getRequestDispatcher("/WEB-INF/jsp/contact/Contact.jsp").forward(request, response);
		} else if (pathInfo != null && pathInfo.contains("/contact-list")) {
		//} else {
			List<ContactViewDto> contacts = contactService.findAllDetailed();
			request.setAttribute("contacts", contacts);
			request.getRequestDispatcher("/WEB-INF/jsp/contact/Contacts.jsp").forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
