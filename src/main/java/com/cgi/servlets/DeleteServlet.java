package com.cgi.servlets;

import java.io.IOException;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cgi.api.PersonCreateViewDto;
import com.cgi.api.PersonDetailedViewDto;
import com.cgi.service.PersonService;

/**
 * Servlet implementation class DeleteServlet
 */
@WebServlet(name = "DeleteServlet", urlPatterns = { "/delete/*" })
public class DeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	@EJB
	private PersonService personService;
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DeleteServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String pathInfo = request.getServletPath() + request.getPathInfo();

		System.out.println("MOJE PATH INFO " + pathInfo);
		
		if (pathInfo != null && pathInfo.contains("/deldetail")) {

			String idStr = request.getParameter("id");
			Long idLong = Long.parseLong(idStr);
			System.out.println(idLong);

			personService.deleteUser(idLong);

			request.getRequestDispatcher("/WEB-INF/jsp/person/Delete.jsp").forward(request, response);
		} else {

			String idStr = request.getParameter("id");
			Long idLong = Long.parseLong(idStr);
			System.out.println(idLong);

			personService.deleteUser(idLong);

			request.getRequestDispatcher("/WEB-INF/jsp/person/Delete.jsp").forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		request.getRequestDispatcher("/WEB-INF/jsp/person/Delete.jsp").forward(request, response);
	}

}
