package com.cgi.servlets;

import java.io.IOException;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cgi.api.PersonCreateViewDto;
import com.cgi.service.PersonService;

/**
 * Servlet implementation class CreateServlet
 */
@WebServlet(name = "CreateServlet", urlPatterns = { "/create" })
public class CreateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@EJB
	private PersonService personService;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public CreateServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.getRequestDispatcher("/WEB-INF/jsp/person/Create.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// check if the user can be authenticated...
		String nickname = request.getParameter("nickname");
		String first_name = request.getParameter("first_name");
		String surname = request.getParameter("surname");

		System.out.println("nickname is: " + nickname);
		System.out.println("first_name is: " + first_name);
		System.out.println("surname is: " + surname);
		
		PersonCreateViewDto pcd = new PersonCreateViewDto();
		pcd.setName(nickname);
		pcd.setFamilyName(surname);
		pcd.setGivenName(first_name);
		
		personService.createUser(pcd);

		request.getRequestDispatcher("/WEB-INF/jsp/person/Create.jsp").forward(request, response);
	}

}
