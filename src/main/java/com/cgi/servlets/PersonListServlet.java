package com.cgi.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cgi.api.PersonBasicViewDto;
import com.cgi.api.PersonDetailedViewDto;
import com.cgi.service.PersonService;

/**
 * Servlet implementation class PersonListServlet
 */

@WebServlet(name = "PersonListServlet", urlPatterns = { "/person-list/*" })
public class PersonListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@EJB
	private PersonService personService;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public PersonListServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String pathInfo = request.getServletPath() + request.getPathInfo();

		System.out.println("MOJE PATH INFO " + pathInfo);
		
		if (pathInfo != null && pathInfo.contains("/detail")) {
			System.out.println("Id...: " + request.getParameter("id"));
			String idStr = request.getParameter("id");
			Long idLong = Long.parseLong(idStr);
			System.out.println(idLong);
			PersonDetailedViewDto person = personService.findById(idLong);
			request.setAttribute("person", person);
			request.getRequestDispatcher("/WEB-INF/jsp/person/Person.jsp").forward(request, response);
		} else if (pathInfo != null && pathInfo.contains("/person-list")) {
		//} else {
			List<PersonBasicViewDto> persons = personService.findAll();
			request.setAttribute("persons", persons);
			request.getRequestDispatcher("/WEB-INF/jsp/person/PersonList.jsp").forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
