package com.cgi.servlets;

import java.io.IOException;
import java.util.List;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cgi.api.PersonBasicViewDto;
import com.cgi.service.PersonService;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet(name = "LoginServlet", urlPatterns = { "/login" })
public class LoginServlet extends HttpServlet {

	@EJB
	private PersonService personService;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public LoginServlet() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.getRequestDispatcher("/WEB-INF/jsp/auth/Login.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// check if the user can be authenticated...
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		
		System.out.println("Username is: " + username);
		System.out.println("Password is: " + password);

		// redirect to the home page..

		request.getRequestDispatcher("/index.jsp").forward(request, response);
//		List<PersonBasicViewDto> persons = personService.findAll();
//		request.setAttribute("persons", persons);
//		request.getRequestDispatcher("/WEB-INF/jsp/person/PersonList.jsp").forward(request, response);
	}

}
