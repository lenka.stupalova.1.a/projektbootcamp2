package com.cgi.api;

import java.sql.Date;

/**
 * @author CGI spojene tabulky contact, contact_type a user
 */
public class ContactDetailedSingleViewDto {
	int id_contact;
	String contact;
	String nickname;
	Date birthday;
	String title; // title of contact type

	public int getId_contact() {
		return id_contact;
	}

	public void setId_contact(int id_contact) {
		this.id_contact = id_contact;
	}

	public String getContact() {
		return contact;
	}

	public void setContact(String contact) {
		this.contact = contact;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

}
