package com.cgi.api;

import java.util.List;

public class AdressInfoView {

	private List<AdressBasicViewAto> adressBasicViewAto;
	private List<AdressPersonGroupByCity> adressPersonGroupByCity;

	public List<AdressBasicViewAto> getAdressBasicViewAto() {
		return adressBasicViewAto;
	}

	public void setAdressBasicViewAto(List<AdressBasicViewAto> adressBasicViewAto) {
		this.adressBasicViewAto = adressBasicViewAto;
	}

	public List<AdressPersonGroupByCity> getAdressPersonGroupByCity() {
		return adressPersonGroupByCity;
	}

	public void setAdressPersonGroupByCity(List<AdressPersonGroupByCity> adressPersonGroupByCity) {
		this.adressPersonGroupByCity = adressPersonGroupByCity;
	}

}
