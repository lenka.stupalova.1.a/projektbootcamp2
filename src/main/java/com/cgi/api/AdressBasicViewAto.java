package com.cgi.api;

public class AdressBasicViewAto {

	private Long id_address;
	private String city;
	private String street;
	private int house_number;
	private String zip_code;

	public Long getId_address() {
		return id_address;
	}

	public void setId_address(Long id_address) {
		this.id_address = id_address;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public int getHouse_number() {
		return house_number;
	}

	public void setHouse_number(int house_number) {
		this.house_number = house_number;
	}

	public String getZip_code() {
		return zip_code;
	}

	public void setZip_code(String zip_code) {
		this.zip_code = zip_code;
	}

}
