package com.cgi.api;


/**
 * @author CGI
 * spojene tabulky contact a contact_type
 */
public class ContactViewDto {
	int id_contact;
	String contact;
	int id_user;
	// int id_contact_type;
	String title; // title of contact type

	public int getId_contact() {
		return id_contact;
	}

	public void setId_contact(int id_contact) {
		this.id_contact = id_contact;
	}

	public String getContact() {
		return contact;
	}

	public void setContact(String contact) {
		this.contact = contact;
	}

	public int getId_user() {
		return id_user;
	}

	public void setId_user(int id_user) {
		this.id_user = id_user;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

}
