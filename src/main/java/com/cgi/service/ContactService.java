package com.cgi.service;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import com.cgi.api.ContactDetailedSingleViewDto;
import com.cgi.api.ContactTypesCount;
import com.cgi.api.ContactViewDto;
import com.cgi.data.ContactDao;

@Stateless
public class ContactService {
	
	@EJB
	private ContactDao contactDao;
	
	public List<ContactViewDto> findAll(){
		
		return contactDao.findAll();
	}
	
	public List<ContactViewDto> findAllDetailed(){
		
		return contactDao.findAllDetailed();
	}
	
	public ContactDetailedSingleViewDto findByIdDetailed(long id) {
		
		return contactDao.findByIdDetailed(id);
	}

	public List<ContactTypesCount> countTypesOfEmail() {
		
		return contactDao.countTypesOfEmail();
	}
	
	
		
		
	

}
