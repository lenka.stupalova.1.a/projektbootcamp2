package com.cgi.service;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import com.cgi.api.AdressBasicViewAto;
import com.cgi.api.AdressBasicViewDto;
import com.cgi.api.AdressInfoView;
import com.cgi.data.AdressDao;
import com.cgi.data.PersonDao;

@Stateless
public class AdressService {
	
	@EJB
	private AdressDao adressDao;
	
	public List<AdressBasicViewDto> findAll(){
		
		
		AdressBasicViewDto city = new AdressBasicViewDto();
		city.setId(1L);
		city.setCity("Brno");
		
		AdressBasicViewDto city2 = new AdressBasicViewDto();
		city2.setId(1L);
		city2.setCity("Bratislava");
		
		List<AdressBasicViewDto> cities = new ArrayList<>();
		cities.add(city);
		cities.add(city2);
		
		return cities;
		
	}
	
	public AdressBasicViewDto findById(Long id) {
		
		AdressBasicViewDto city = new AdressBasicViewDto();
		city.setId(1L);
		city.setCity("Praha");
		
		return city;
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public AdressInfoView adressInfoView() {
		AdressInfoView a = new AdressInfoView();
		a.setAdressBasicViewAto( adressDao.findAll()  );
		a.setAdressPersonGroupByCity( adressDao.findCounts()  );
		
		return a;
	}
}
