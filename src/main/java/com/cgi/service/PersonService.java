package com.cgi.service;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import com.cgi.api.PersonDetailedViewDto;
import com.cgi.data.PersonDao;
import com.cgi.api.PersonBasicViewDto;
import com.cgi.api.PersonCreateViewDto;

/**
 * @author CGI
 *
 */
@Stateless
public class PersonService {
	

	@EJB
	private PersonDao personDao;
	
	public List<PersonBasicViewDto> findAll(){
		
		return personDao.findAll();
	}
	
	public PersonDetailedViewDto findById(Long id) {

		return personDao.findById(id);
	}

	public boolean createUser(PersonCreateViewDto pcd) {
		return personDao.createUser(pcd);
		
	}

	public boolean deleteUser(Long id) {
		return personDao.deleteUser(id);
		
	}

	public boolean updateUser(Long id, String surname) {
		return personDao.updateUser(id,surname);
		
	}
	
	

}
