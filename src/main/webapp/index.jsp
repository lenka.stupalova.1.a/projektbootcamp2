<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="my"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page contentType="text/html; charset=UTF-8"%>


<my:pagetemplate title="Welcome to cgi">
	<jsp:attribute name="body">
	<h1 class="page-header">

Úvodná stránka, Vitajte!
	</h1>

<h3>
Skoro všetky stránky, na ktoré sa dá preklikať nejako:
</h3>
<br>
Novopridané kontakty:<br>
<a href="/bootcamp-cgi/contact-list"> Contact info view </a>
		<br>
<a href="/bootcamp-cgi/contact-list/detail?id=1"> Konkrétny kontakt - príklad (osoba s id 1, ak nebola vymazaná už)- 3 tab spojené </a>
		<br>
<a href="/bootcamp-cgi/contact-list/contact-types"> Contact types </a>
<br>
-----------
<br>
<a href="/bootcamp-cgi/person-list"> Persons </a>
<br>
<a href="/bootcamp-cgi/person-list/detail?id=1"> Príklad konkrétnej osoby (osoba s id 1, ak nebola vymazaná už)</a>
<br>
Create person sa nachádza v Persons na spodu // stránka s výpisom Created
<br>
Delete person sa nachádza v Persons vedľa záznamu
<br>
Update person sa nachádza v Persons po rozkliknutí view konkrétnej osoby
<br>
<a href="/bootcamp-cgi/adress-info"> Adress info view a adress count bez Brna </a>
<br>
<a href="/bootcamp-cgi/about"> About page - len text o projekte </a>
<br>
<a href="/bootcamp-cgi/randomlink"> Error page pre neexistujuci link </a>
<br>
<a href="/bootcamp-cgi/login"> Login - aj vpravo hore </a>
		<br>
<a href="${pageContext.request.contextPath}/"> A index </a>


	</jsp:attribute>
</my:pagetemplate>
