<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="my"%>

<%@ page contentType="text/html; charset=UTF-8"%>

<my:pagetemplate title="About">
	<jsp:attribute name="body">
	<h1 class="page-header">
About this project
</h1>

Tento projekt bol vytvorený ako výstup z dvojýžňového školenia GOPAS - Programovaní.
<br>
Jedná sa o weblogic aplikáciu, ktorá pracuje s MySQL databázou.
<br>
Zadanie projektu si môžete pozreť :
<a href="<c:url value="/assets/pdfs/Bootcamp-cgi-project-2.pdf"/>"> >>TU<< </a>


<h1> Iné projekty školenia
</h1>

Okrem práce s csv súbormi či vytváraním vlastného kalendáru a práce s Date, 
sme mali odovzdať už aj prvú samostatnú úlohu - commandlinovú hru. Link na prvý "projekt":
<a href="https://gitlab.com/lenka.stupalova.1.a/hladaniepokladu3"> >>TU<< </a>

	</jsp:attribute>
</my:pagetemplate>