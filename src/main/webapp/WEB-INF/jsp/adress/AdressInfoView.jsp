<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="my"%>

<%@ page contentType="text/html; charset=UTF-8"%>

<my:pagetemplate title="Contacts">
	<jsp:attribute name="body">
	<h1 class="page-header">
		List of all adresses in the system <small></small>
	</h1>
	<p>
	Dole pod tabuľkou sa nachádza počet ľudí v jednotlivých mestách!
	</p>
	<table class="table table-striped">
		<thead>
			<tr>
				<th>id</th>
				<th>city</th>
				<th>street</th>
				<th>house nb</th>
				<th>zip code</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${aiv}" var="aiv">
				<tr>
					<td><c:out value="${aiv.id_address}" /></td>
					<td><c:out value="${aiv.city}" /></td>
					<td><c:out value="${aiv.street}" /></td>
					<td><c:out value="${aiv.house_number}" /></td>
					<td><c:out value="${aiv.zip_code}" /></td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
	<br>
	<br>
	
	<h2>
	Počet našich obyvateľov v jednotlivých mestách
	</h2>
	
	<p>
	Z adries sme nezverejnili údaje pre mesto Brno, na základe miestneho zákona o ochrane osobných údajov mesta :D
	</p>
	
	
	<table class="table table-striped">
		<thead>
			<tr>
				<th>city</th>
				<th>count</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${aiv2}" var="aiv2">
				<c:if test="${aiv2.city != 'Brno'}">
					<tr>
						<td><c:out value="${aiv2.city}" /></td>
						<td><c:out value="${aiv2.userCount}" /></td>
					</tr>
				</c:if>

			</c:forEach>
		</tbody>
	</table>
	</jsp:attribute>
</my:pagetemplate>