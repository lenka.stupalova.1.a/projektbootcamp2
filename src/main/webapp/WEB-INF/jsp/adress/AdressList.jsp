<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="my"%>

<%@ page contentType="text/html; charset=UTF-8"%>

<my:pagetemplate title="Contacts">
	<jsp:attribute name="body">
	<h1 class="page-header">
		List of all adresses in the system <small></small>
	</h1>

    <table class="table table-striped">
		<thead>
			<tr>
				<th>id</th>
				<th>city</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${cities}" var="city">
				<tr>
					<td><c:out value="${city.id}" /></td>
					<td><c:out value="${city.city}" /></td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
	</jsp:attribute>
</my:pagetemplate>