<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="my"%>
<%@ page contentType="text/html; charset=UTF-8"%>

<my:pagetemplate title="ContactTypy">
	<jsp:attribute name="body">
	<h1 class="page-header">
		List of count of types of contacts<small></small>
	</h1>
	<table class="table table-striped">
		<thead>
			<tr>
				<th>id typu</th>
				<th>typ contactu</th>
				<th>pocet</th>
				
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${contypes}" var="contact">
				<tr>
					<td><c:out value="${contact.id_contact}" /></td>
					<td><c:out value="${contact.title}" /></td>
					<td><c:out value="${contact.count}" /></td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
	</jsp:attribute>
</my:pagetemplate>