<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="my"%>

<%@ page contentType="text/html; charset=UTF-8"%>

<my:pagetemplate title="Contacts">
	<jsp:attribute name="body">
	<h1 class="page-header">
		List of all contacts in the system <small></small>
	</h1>
<p>
		Click on View button to view detailed contact info like contact type, 
		user nickname and user birthday! 
		<br>3 DB tables Joined there!
	</p>
	<p>
			<a
				href="${pageContext.request.contextPath}/contact-list/contact-types"
				class="btn btn-primary"> >>View count of types of contacts<< </a>
	</p>
	<table class="table table-striped">
		<thead>
			<tr>
				<th>id</th>
				<th>contact</th>
				<th>user id</th>
				<th>typ contactu</th>
				
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${contacts}" var="contact">
				<tr>
					<td><c:out value="${contact.id_contact}" /></td>
					<td><c:out value="${contact.contact}" /></td>
					<td><c:out value="${contact.id_user}" /></td>
					<td><c:out value="${contact.title}" /></td>
					<td><a
							href="${pageContext.request.contextPath}/contact-list/detail?id=${contact.id_contact}"
							class="btn btn-primary">view</a></td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
	</jsp:attribute>
</my:pagetemplate>