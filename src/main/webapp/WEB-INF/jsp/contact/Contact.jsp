<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="my"%>
<%@ page contentType="text/html; charset=UTF-8"%>

<my:pagetemplate title="Contact">
	<jsp:attribute name="body">
	<table class="table table-striped">
		<thead>
			<tr>
				<th>id contact </th>
				<th>contact </th>
				<th>contact type </th>
				<th>nickname </th>
				<th>birthday </th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td><c:out value="${contact.id_contact}" /></td>
				<td><c:out value="${contact.contact}" /></td>
				<td><c:out value="${contact.title}" /></td>
				<td><c:out value="${contact.nickname}" /></td>
				<td><c:out value="${contact.birthday}" /></td>
			</tr>
		</tbody>
	</table>
	</jsp:attribute>
</my:pagetemplate>