<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="my"%>
<%@ page contentType="text/html; charset=UTF-8"%>

<my:pagetemplate title="Persons">
	<jsp:attribute name="body">
	<h1 class="page-header">
		List of all persons in the system <small></small>
	</h1>
	<h3>
		Na spodu tejto stránky sa nachádza formulár na vytvorenie nového používateľa
	</h3>
	<table class="table table-striped">
		<thead>
			<tr>
				<th>id</th>
				<th>name</th>
				<th>given name</th>
				<th>family name</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${persons}" var="person">
				<tr>
					<td><c:out value="${person.id}" /></td>
					<td><c:out value="${person.name}" /></td>
					<td><c:out value="${person.givenName}" /></td>
					<td><c:out value="${person.familyName}" /></td>
					<td><a
							href="${pageContext.request.contextPath}/person-list/detail?id=${person.id}"
							class="btn btn-primary">View</a></td>
							<td><a
							href="${pageContext.request.contextPath}/delete/deldetail?id=${person.id}"
							class="btn btn-primary">Delete</a></td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
	
	<div class="form-group">
				<p>
								
		
		
		<h3>
									<strong>Create user</strong>
								</h3>
							<c:url value="/create" var="createUrl" />
							<form role="form" action="${createUrl}" method="post"
			class="create-form">
								<div class="form-group">
									<label class="sr-only" for="nickname">Nickname</label> <input
					type="text" name="nickname" placeholder="nickname.."
					class="form-username form-control" id="form-nickname">
								</div>
								<div class="form-group">
									<label class="sr-only" for="first_name">First name</label> <input
					type="text" name="first_name" placeholder="first_name.."
					class="form-username form-control" id="form-first_name">
								</div>
								<div class="form-group">
									<label class="sr-only" for="surname">Surname</label> <input
					type="text" name="surname" placeholder="surname.."
					class="form-username form-control" id="form-surname">
								</div>
								<input type="hidden" name="${_csrf.parameterName}"
				value="${_csrf.token}" />
								<button type="submit" class="btn">Create!</button>
							</form>
						</div>
		
		</p>
		
	</div>
	
	</jsp:attribute>
</my:pagetemplate>