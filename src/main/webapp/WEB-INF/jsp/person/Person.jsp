<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="my"%>
<%@ page contentType="text/html; charset=UTF-8"%>

<my:pagetemplate title="Contact">
	<jsp:attribute name="body">
    <table class="table table-striped">
        <thead>
            <tr>
                <th>id</th>
                <th>name</th>
                <th>given name</th>
                <th>family name</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><c:out value="${person.id}" /></td>
                <td><c:out value="${person.name}" /></td>
                <td><c:out value="${person.givenName}" /></td>
                <td><c:out value="${person.familyName}" /></td>
            </tr>
        </tbody>
    </table>
    
    <div class="form-group">
    <p>
    <h3>
    Vydala som sa a chcem upraviť priezvisko:
    </h3>
							<c:url value="/update" var="updateUrl" />
					
		<form role="form" action="${updateUrl}" method="post"
			class="update-form">
			<div class="form-group">
									<label class="sr-only" for="id">Id</label> <input
					type="text" name="id" value="${person.id}"
					class="form-username form-control" id="form-id" readonly visibility="hidden">
								</div>
								<div class="form-group">
									<label class="sr-only" for="surname">Surname</label> <input
					type="text" name="surname" placeholder="${person.familyName}"
					class="form-username form-control" id="form-surname">
								</div>
								<input type="hidden" name="${_csrf.parameterName}"
				value="${_csrf.token}" />
								<button type="submit" class="btn">Update Surname!</button>
							</form>
						</div>
    </p>
    </div>
    
    </jsp:attribute>
</my:pagetemplate>