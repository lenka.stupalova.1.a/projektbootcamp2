<%@ tag pageEncoding="UTF-8" dynamic-attributes="dynattrs"%>
<%@ attribute name="title" required="false"%>
<%@ attribute name="subtitle" required="false"%>
<%@ attribute name="head" fragment="true"%>
<%@ attribute name="body" fragment="true" required="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="f" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html>
<html lang="${pageContext.request.locale}">
<!--  head starts -->
<head>
<meta charset="UTF-8">
<!-- CSS -->
<link rel="stylesheet"
	href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
<link rel="stylesheet"
	href="<c:url value="/assets/bootstrap/css/bootstrap.min.css"/>">
<link rel="stylesheet"
	href="<c:url value="/assets/font-awesome/css/font-awesome.min.css"/>">
<link rel="stylesheet"
	href="<c:url value="/assets/css/form-elements.css"/>">
<link rel="stylesheet" href="<c:url value="/assets/css/style.css"/>">

<!-- Favicon and touch icons -->
<link rel="shortcut icon"
	href="<c:url value="/assets/ico/favicon.png"/>">
<link rel="apple-touch-icon-precomposed" sizes="144x144"
	href="<c:url value="/assets/ico/apple-touch-icon-144-precomposed.png"/>">
<link rel="apple-touch-icon-precomposed" sizes="114x114"
	href="<c:url value="/assets/ico/apple-touch-icon-114-precomposed.png"/>">
<link rel="apple-touch-icon-precomposed" sizes="72x72"
	href="<c:url value="/assets/ico/apple-touch-icon-72-precomposed.png"/>">
<link rel="apple-touch-icon-precomposed"
	href="<c:url value="/assets/ico/apple-touch-icon-57-precomposed.png"/>">
<!--//end-animate-->
<jsp:invoke fragment="head" />
<meta charset="UTF-8">
</head>

<body>
	<!--header-->
	<div class="header">
		<nav class="navbar navbar-inverse">
			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="container-fluid">
				<div class="navbar-header">
					<a class="navbar-brand" href="${pageContext.request.contextPath}/">Bootcamp
						CGI DEMO</a>
				</div>
				<ul class="nav navbar-nav">
					<li><a href="${pageContext.request.contextPath}/person-list">Persons</a></li>
					<li><a href="${pageContext.request.contextPath}/adress-info">Adresses</a></li>
					<li><a href="${pageContext.request.contextPath}/about">About</a></li>
					<li><a href="${pageContext.request.contextPath}/random">Na error</a></li>
					<li><a href="${pageContext.request.contextPath}/contact-list">Contacts</a></li>
				</ul>

				<ul class="nav navbar-nav navbar-right">
					<li><a href="${pageContext.request.contextPath}/login"><span
							class="glyphicon glyphicon-log-in"> Login</span></a></li>
				</ul>
			</div>
		</nav>
	</div>
	<!--//header-->

	<div class="container">
		<!-- page body -->
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				<!-- page body -->
				<jsp:invoke fragment="body" />
			</div>
		</div>
	</div>
</body>
</html>